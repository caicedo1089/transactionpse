(
	function()
	{
		//Creamos el Sonic Loader
		var sonicLoader = new Sonic({
 
			width: 50,
			height: 50,
			padding: 50,
		 
			strokeColor: '#000',
		 
			pointDistance: .01,
			stepsPerFrame: 3,
			trailLength: .7,
		 
			step: 'fader',
		 
			setup: function() {
				this._.lineWidth = 5;
			},
		 
			path: [
				['arc', 20, 20, 20, 0, 360]
			]
		 
		});
		 
		sonicLoader.play();
		
		//Verificamos que este ejecutando GoogleChrome
		if( navigator && 
			navigator['vendor'] != undefined && 
			navigator['vendor'] != 'Google Inc.' )
		{
			alert('No se soporta ese Browser, sólo Google Chrome');
			return;
		}
		
		var strURL = 'http://localhost:8000/'

		//Seteamos los path
		Ext.Loader.setConfig(
			{
				enabled: true,
				paths: {
					Ext: 'js/ext4.2.1/src',
				}
			}
		);

		//Precargamos los librerías
		Ext.require([
			'Ext.grid.*',
			'Ext.form.*',
			'Ext.data.*',
			'Ext.panel.*'
		]);
		
		//Cargamos la aplicación
		Ext.onReady(
			function()
			{
				//Agregamos el Sonic Loader
				var container = document.getElementsByClassName('content');
				container = container[0] || null;
				
				if(container != null)
				{
					var title = document.getElementsByClassName('title');
					container.removeChild(title[0] || null);  
					container.appendChild(sonicLoader.canvas);
				}
			}
		);

		//Validamos que no sea una respuesta
		if(window.location.search.indexOf('?reference=') != -1)
		{
			let reference = window.location.search.split('?reference=').join('')

			Ext.application({
				requires: [
					'Ext.container.Viewport'
				],
				name: 'AppExt',
				controllers: [],
				views: [],
				appFolder: 'app/',
				launch: function ()
				{
					let principalWindow = Ext.create('Ext.container.Viewport', {
						layout: 'fit',
						counterId: 0,
						items: [
							{
								xtype: 'panel',
								title: 'Estado final de la transacción',
								layout: 'fit',
								items:[
									{
										xtype:'label',
										id: 'idBodyResponse',
										html:`reference: ${reference}`
									}
								]
							}
						],
						listeners:{
							beforerender: function ( thisViewport, eOpts )
							{
								var container = document.getElementsByClassName('container');
								document.body.removeChild(container[0] || null);  
							}
						}
					})

					let dataTransactions = window.localStorage.getItem('transactions')
					if(!Ext.isEmpty(dataTransactions))
					{
						dataTransactions = Ext.JSON.decode(dataTransactions)
					}

					let model = null
					for (let key in dataTransactions) 
					{
						if (dataTransactions.hasOwnProperty(key)) 
						{
							let element = dataTransactions[key];
							
							if(element.information.reference == reference)
							{
								model = element
								break
							}
						}
					}

					if(!Ext.isEmpty(model))
					{
						//Actualizamos el la información de la transacción
						Ext.Ajax.request({
							url: `${strURL}/gettransactioninformation/${model.create.transactionID}`,
							method: 'GET',
							success: function(response)
							{
								let objResp = Ext.JSON.decode(response.responseText)
								let text = ''
								
								if(objResp.data)
								{
									for (let key in objResp.data) 
									{
										if (objResp.data.hasOwnProperty(key)) 
										{
											let element = objResp.data[key];
											text += `${key}: ${element},\n`
										}
									}
								}
								
								let labelCmp = Ext.getCmp('idBodyResponse')
								labelCmp.update(
									`<div style="padding:10px 20px;"><pre>${text}</pre></div>`
								)
							},
							failure: function(response)
							{
								let objResp = Ext.JSON.decode(response.responseText)

								console.log('Hay un error:', dataCreate.transactionID, objResp)
							}
						})
					}
				}
			})
			return
		}

		var storeTransaction = new Ext.data.JsonStore({
			autoLoad: false,
			remoteSort : false,
			idProperty:'transactionID',
			fields: ['create', 'information'],
		});

		console.log('storeTransaction:', storeTransaction)
		
		var dataTransactions = window.localStorage.getItem('transactions')
		if(!Ext.isEmpty(dataTransactions))
		{
			dataTransactions = Ext.JSON.decode(dataTransactions)
			let dataAux = []
			for (let key in dataTransactions) 
			{
				if (dataTransactions.hasOwnProperty(key)) 
				{
					let transaction = dataTransactions[key];
					dataAux.push(transaction)
				}
			}
			storeTransaction.loadData(dataAux)
		}
		else
		{
			dataTransactions = {}
		}

		var storeBanks = new Ext.data.JsonStore({
			idProperty:'bankCode',
			fields: ['bankCode', 'bankName']
		})

		//Cargamos los bancos
		Ext.Ajax.request({
			url: `${strURL}/banks`,
			method: 'GET',
			success: function(response)
			{
				let objResp = Ext.JSON.decode(response.responseText)
				
				storeBanks.loadData(objResp.data)
			}
		})
		var allDataPCTest = window.localStorage.getItem('allDataPCTest')

		//Manejo de las tareas periodicas
		function updateTransaction(model)
		{
			let dataCreate = model.get('create')
			let dataInformation = model.get('information')
			
			if( dataInformation.transactionState != 'OK' && 
				dataInformation.transactionState != 'NOT_AUTHORIZED' && 
				dataInformation.transactionState != 'FAILED')
			{
				//Actualizamos el la información de la transacción
				Ext.Ajax.request({
					url: `${strURL}/gettransactioninformation/${dataCreate.transactionID}`,
					method: 'GET',
					success: function(response)
					{
						let objResp = Ext.JSON.decode(response.responseText)

						if(!objResp.error)
						{
							model.set('information', objResp.data)
							dataTransactions[dataCreate.transactionID].information = objResp.data
							window.localStorage.setItem('transactions', Ext.JSON.encode(dataTransactions))
						}
						else
						{
							console.log('Hay un error:', dataCreate.transactionID, objResp)
						}
					},
					failure: function(response)
					{
						let objResp = Ext.JSON.decode(response.responseText)

						console.log('Hay un error:', dataCreate.transactionID, objResp)
					}
				})
			}
		}
		var runner = new Ext.util.TaskRunner();
		var task = runner.newTask({
			run: function(count) 
			{
				storeTransaction.each(updateTransaction)
			},
			interval: 30000
		});
		task.start();

		//Ventana de Creación
		var windowCreate = Ext.create('Ext.window.Window', {
			title: 'Crear Transacción',
			height: 600,
			width: 400,
			modal: true,
			layout:{
				type: 'vbox',
				align : 'stretch',
				pack  : 'start',
			},
			closeAction: 'hide',
			
			items: [
				{
					//flex:1,
					xtype: 'form',
					name: 'general',
					layout:'fit',
					frame: false,
					items: [
						{
							xtype: 'fieldset',
							title: 'Datos Generales',
							collapsible: false,
							layout:{
								type: 'vbox',
								align : 'stretch',
								pack  : 'start',
							},
							defaultType: 'textfield',
							defaults:{
								padding: '2 5',
								allowBlank: false,
							},
							margin:'5',
							items:[
								{
									xtype:'combo',
									fieldLabel: 'Tipo de Cuenta',
									name: 'bankInterface',
									store: new Ext.data.JsonStore({
										idProperty:'id',
										fields: ['id', 'value'],
										data: [
											{
												id:'0', 
												value:'Personas'
											},	
											{
												id:'1', 
												value:'Empresas'
											},	
										]
									}),
									queryMode: 'local',
									displayField: 'value',
									valueField: 'id',
									editable: false,
								},
								{
									xtype:'combo',
									fieldLabel: 'Entidad Bancaria',
									name: 'bankCode',
									store: storeBanks,
									queryMode: 'local',
									displayField: 'bankName',
									valueField: 'bankCode',
									editable: false,
								},
								{
									xtype: 'hidden',
									name:'language',
									value: 'ES'
								},
								{
									xtype: 'hidden',
									name:'currency',
									value: 'COP'
								},
								{
									xtype     : 'textareafield',
									name      : 'description',
									fieldLabel: 'Descripción',
								},
								{
									xtype: 'numberfield',
									name: 'totalAmount',
									fieldLabel: 'Total',
									minValue: 1,
								},
								{
									xtype: 'numberfield',
									name: 'taxAmount',
									fieldLabel: 'Impuestos',
									minValue: 1,
								},
								{
									xtype: 'numberfield',
									name: 'devolutionBase',
									fieldLabel: 'Base de Devolución',
									minValue: 1,
								},
								{
									xtype: 'numberfield',
									name: 'tipAmount',
									fieldLabel: 'Propina',
									minValue: 0,
								},
							]
						},
					],
				},
				{
					flex:1,
					xtype:'form',
					layout:'fit',
					defaults:{
						padding: '2 5',
						allowBlank: false,
					},
					name: 'person',	
					items: [
						{
							xtype: 'fieldset',
							title: 'Datos del Comprador/Pagador/Envío',
							collapsible: false,
							layout:{
								type: 'vbox',
								align : 'stretch',
								pack  : 'start',
							},
							margin:'5',
							autoScroll: true,
							items:[
								{
									xtype:'combo',
									fieldLabel: 'Tipo de Documento',
									name: 'documentType',
									store: new Ext.data.JsonStore({
										idProperty:'id',
										fields: ['id', 'value'],
										data: [
											{ id:'CC', value:'Cédula de Ciudadanía'},	
											{ id:'CE', value:'Cédula de Extrangería'},
											{ id:'TI', value:'Tarjeta de Identidad'},
											{ id:'PPN', value:'Pasaporte'},
										]
									}),
									queryMode: 'local',
									displayField: 'value',
									valueField: 'id',
									editable: false,
								},
								{
									xtype:'textfield',
									fieldLabel: 'Documento',
									name:'document',
								},
								{
									xtype:'textfield',
									fieldLabel: 'Nombres',
									name:'firstName',
								},
								{
									xtype:'textfield',
									fieldLabel: 'Apellidos',
									name:'lastName',
								},
								{
									xtype:'textfield',
									fieldLabel: 'Compañía',
									name:'company',
								},
								{
									xtype:'textfield',
									fieldLabel: 'E-Mail',
									name:'emailAddress',
								},
								{
									xtype     : 'textareafield',
									name      : 'address',
									fieldLabel: 'Dirección',
								},
								{
									xtype:'textfield',
									fieldLabel: 'Ciudad',
									name:'city',
								},
								{
									xtype:'textfield',
									fieldLabel: 'Provincia',
									name:'province',
								},
								{
									xtype:'combo',
									fieldLabel: 'País',
									name: 'country',
									store: new Ext.data.JsonStore({
										idProperty:'id',
										fields: ['id', 'value'],
										data: [
											{ id:'CO', value:'Colombia'},	
										]
									}),
									queryMode: 'local',
									displayField: 'value',
									valueField: 'id',
									editable: false,
								},
								{
									xtype:'textfield',
									fieldLabel: 'Teléfono',
									name:'phone',
								},
								{
									xtype:'textfield',
									fieldLabel: 'Celular',
									name:'mobile',
								},
							]
						}
					],
					buttons:[
						{
							text: 'Crear',
							handler: function() 
							{
								let formPanelGeneral = windowCreate.down('form[name=general]')
								let formGeneral = formPanelGeneral.getForm()
								let formPanelPerson = windowCreate.down('form[name=person]')
								let formPerson = formPanelPerson.getForm()

								if(formGeneral.isValid() && formPerson.isValid())
								{
									windowCreate.getEl().mask('Creando la transacción...')
									
									let valuesGeneral = formGeneral.getValues()
									let valuesPerson = formPerson.getValues()

									valuesGeneral['reference'] = `PC-${Ext.Date.format(new Date(), 'timestamp')}`
									valuesGeneral['payer'] = valuesPerson
									valuesGeneral['buyer'] = valuesPerson
									valuesGeneral['shipping'] = valuesPerson
									
									window.localStorage.setItem('allDataPCTest', Ext.JSON.encode(valuesGeneral))
									//Hacemos la llamada Ajax
									Ext.Ajax.request({
										url: `${strURL}/createtransaction`,
										method: 'POST',
										jsonData: valuesGeneral,
										success: function(response)
										{
											let objResp = Ext.JSON.decode(response.responseText)
											
											if(!objResp.error)
											{
												let transaction = {
													create: objResp.data,
													information: {}
												}

												dataTransactions[transaction.create.transactionID] = transaction
												
												window.localStorage.setItem('transactions', Ext.JSON.encode(dataTransactions))
												storeTransaction.add(transaction)

												//Se actualiza la información del último modelo
												updateTransaction(storeTransaction.last())
												
												formGeneral.reset()
												formPerson.reset()
												windowCreate.hide()

												if(!Ext.isEmpty(objResp.data.bankURL))
												{
													Ext.create('Ext.window.Window', {
														title: 'Página del Banco',
														height: 600,
														width: 800,
														modal: true,
														layout:{
															type: 'vbox',
															align : 'stretch',
															pack  : 'start',
														},
														closeAction: 'hide',
														
														items: [
															{
																flex: 1,
																xtype: 'label',
																html: `<iframe src="${objResp.data.bankURL}" height="100%" width="100%"></iframe>`
															}
														]
													}).show()
												}
											}

											windowCreate.getEl().unmask()

											Ext.Msg.show({
												title : 'Respuesta',
												msg : `${objResp.error? objResp.msg : 'OK'}`,
												buttons : Ext.Msg.OK,
												icon : Ext.Msg[objResp.error? 'ERROR' : 'INFO']
											});
										},
										failure: function(response)
										{
											let objResp = Ext.JSON.decode(response.responseText)
											
											windowCreate.getEl().unmask()

											Ext.Msg.show({
												title : 'Error',
												msg : `${objResp.msg}`,
												buttons : Ext.Msg.OK,
												icon : Ext.Msg.ERROR
											});
										}
									})
								}
							}
						},
						{
							text:'test',
							handler:function()
							{
								let formPanelGeneral = windowCreate.down('form[name=general]')
								let formGeneral = formPanelGeneral.getForm()
								let formPanelPerson = windowCreate.down('form[name=person]')
								let formPerson = formPanelPerson.getForm()

								let data = window.localStorage.getItem('allDataPCTest')

								if(!Ext.isEmpty(data))
								{
									data = Ext.JSON.decode(data)
									formGeneral.setValues(data)
									formPerson.setValues(data.payer)
								}
							}
						},
						{
							text: 'Cancel',
							handler: function() 
							{
								windowCreate.hide()
							}
						},
					]
				}
			]
		})

		Ext.application({
			requires: [
				'Ext.container.Viewport'
			],
			name: 'AppExt',
			controllers: [],
			views: [],
			appFolder: 'app/',
			launch: function ()
			{
				//Habilitar Cors 
				Ext.Ajax.useDefaultXhrHeader = false;
				Ext.Ajax.cors = true;
				
				//Desplegamos la aplicación
				var principalWindow = Ext.create('Ext.container.Viewport', {
					layout: 'fit',
					counterId: 0,
					items: [
						{
							layout: {
								type: 'border'
							},
							items: [
								{
									region: 'center',
									xtype: 'panel',
									layout: 'fit',
									border: false,
									width: '100%',
									items: [
										{
											xtype: 'grid',
											store: storeTransaction,
											selType: 'checkboxmodel' ,
											viewConfig:{
												enableTextSelection: true,
											},
											columns: [
												{ 
													width: 80,
													text: 'transactionID',  
													dataIndex: 'create',
													renderer: function(value, meta, record)
													{
														return value.transactionID || 'Error'
													}
												},
												{
													text: 'Creación',
													columns: [
														{
															text: 'returnCode',
															dataIndex: 'create',
															renderer: function(value, meta, record)
															{
																return value.returnCode || 'Error'
															}
														}, 
														{
															width: 90,
															text: 'trazabilityCode',
															dataIndex: 'create',
															renderer: function(value, meta, record)
															{
																return value.trazabilityCode || 'Error'
															}
														},
														{
															width: 100,
															text: 'transactionCycle',
															dataIndex: 'create',
															renderer: function(value, meta, record)
															{
																return value.transactionCycle || 'Error'
															}
														},
														{
															text: 'sessionID',
															dataIndex: 'create',
															renderer: function(value, meta, record)
															{
																meta.tdAttr = `data-qtip="${value.sessionID}"`
																return value.sessionID || 'Error'
															}
														},
														{
															text: 'bankURL',
															dataIndex: 'create',
															renderer: function(value, meta, record)
															{
																meta.tdAttr = `data-qtip="${value.bankURL}"`
																return value.bankURL || 'Error'
															}
														},
														{
															width: 80,
															text: 'bankCurrency',
															dataIndex: 'create',
															renderer: function(value, meta, record)
															{
																return value.bankCurrency || 'Error'
															}
														},
														{
															width: 70,
															text: 'bankFactor',
															dataIndex: 'create',
															renderer: function(value, meta, record)
															{
																return value.bankFactor || 'Error'
															}
														},
														{
															width: 120,
															text: 'responseReasonCode',
															dataIndex: 'create',
															renderer: function(value, meta, record)
															{
																return value.responseReasonCode || 'Error'
															}
														},
														{
															width: 120,
															text: 'responseReasonText',
															dataIndex: 'create',
															renderer: function(value, meta, record)
															{
																meta.tdAttr = `data-qtip="${value.responseReasonText}"`
																return value.responseReasonText || 'Error'
															}
														}
													]
												},
												{ 
													text: 'Información',
													columns: [
														{
															width: 90,
															text: 'reference',  
															dataIndex: 'information',
															renderer: function(value, meta, record)
															{
																return value.reference || 'Procesando'
															}
														},
														{
															width: 150,
															text: 'requestDate',  
															dataIndex: 'information',
															renderer: function(value, meta, record)
															{
																return value.requestDate || 'Procesando'
															}
														},
														{
															width: 150,
															text: 'bankProcessDate',  
															dataIndex: 'information',
															renderer: function(value, meta, record)
															{
																return value.bankProcessDate || 'Procesando'
															}
														},
														{
															width: 50,
															text: 'onTest',  
															dataIndex: 'information',
															renderer: function(value, meta, record)
															{
																return value.onTest || 'Procesando'
															}
														},
														{
															width: 120,
															text: 'transactionState',  
															dataIndex: 'information',
															renderer: function(value, meta, record)
															{
																return value.transactionState || 'Procesando'
															}
														},
														{
															width: 120,
															text: 'responseReasonCode',
															dataIndex: 'information',
															renderer: function(value, meta, record)
															{
																return value.responseReasonCode || 'Error'
															}
														},
														{
															width: 120,
															text: 'responseReasonText',
															dataIndex: 'information',
															renderer: function(value, meta, record)
															{
																meta.tdAttr = `data-qtip="${value.responseReasonText}"`
																return value.responseReasonText || 'Error'
															}
														}
													]
												}
											],
											/*dockedItems: [
												{
													xtype: 'pagingtoolbar',
													store: storeInvoinces,
													dock: 'bottom',
													displayInfo: true
												}
											],*/
										}
									]
								},
								//Menú
								{
									xtype: 'toolbar',
									region: 'north',
									ui: 'footer',
									height: 40,
									items: [
										{
											xtype: 'label',
											html: [
												'<p style="font-size: 18px;font-weight: bold; padding: 0;margin: 5px 10px;">',
													'Transacciones PSE',
												'</p>',
											].join('')
										},
										'->',
										{
											text: 'Crear',
											listeners:{
												click:function(thisButton, pressed, eOpts)
												{
													let formPanelGeneral = windowCreate.down('form[name=general]')
													let formGeneral = formPanelGeneral.getForm()
													let formPanelPerson = windowCreate.down('form[name=person]')
													let formPerson = formPanelPerson.getForm()

													formGeneral.reset()
													formPerson.reset()
													windowCreate.show()
												},
											}
										},
									]
								},
								//Footer
								{
									region: 'south',
									xtype: 'label',
									padding: '5 10',
									html: [
										'<div style="padding: 5 10;">',
											'<div style="float:left;">',
												'Version ' + PVersion,
											'</div>',
											'<div style="float:right;">',
												'Desarrollado por ',
												'<a href="http://pcaicedo.com" target="_black" style="text-decoration: none;">',
													'Pedro Caicedo',
												'</a>',
											'</div>',
										'</div>',
									].join(''),
								},
							]
						}
					],
					listeners:{
						beforerender: function ( thisViewport, eOpts )
						{
							var container = document.getElementsByClassName('container');
							document.body.removeChild(container[0] || null);  
						}
					}
				});

			}
		});
	}
).call(this);
