<?php

namespace App\Soap\Response;

use App\Soap\model\Bank;

class GetBankListResponse
{
  /**
   * @var bank[]
   */
  protected $GetBankListResult;

  /**
   * GetBankListResponse constructor.
   *
   * @param bank[]
   */
  public function __construct($GetBankListResult)
  {
    $this->GetBankListResult = $GetBankListResult;
  }

  /**
   * @return bank[]
   */
  public function getGetBankListResult()
  {
    return $this->GetBankListResult;
  }
}