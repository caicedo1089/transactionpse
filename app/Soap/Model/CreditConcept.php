<?php

namespace App\Soap\Model;

class CreditConcept
{
  /**
   * @var string[12] 
   */
  protected $entityCode;

  /**
   * @var string[12]
   */
  protected $serviceCode;

  /**
   * @var double
   */
  protected $amountValue;

  /**
   * @var double
   */
  protected $taxValue;

  /**
   * @var string[60]
   */
  protected $description;

  /**
   * Attribute constructor.
   *
   * @param string $entityCode
   * @param string $serviceCode
   * @param double $amountValue
   * @param double $taxValue
   * @param string $description
   */
  public function __construct($entityCode, $serviceCode, $amountValue, $taxValue, $description)
  {
    $this->entityCode = $entityCode;
    $this->serviceCode   = $serviceCode;
    $this->amountValue   = $amountValue;
    $this->taxValue   = $taxValue;
    $this->description   = $description;
  }

  /**
   * @return string
   */
  /*public function getBankCode()
  {
    return $this->bankCode;
  }*/

  /**
   * @return string
   */
  /*public function getBankName()
  {
    return $this->bankName;
  }*/
}