<?php

namespace App\Soap\Model;

class PSETransactionResponse
{
  /**
   * @var int
   */
  protected $transactionID;

  /**
   * @var string[32]  
   */
  protected $sessionID;

  /**
   * @var string[30] 
   */
  protected $returnCode;

  /**
   * @var string[40]
   */
  protected $trazabilityCode;

  /**
   * @var int
   */
  protected $transactionCycle;

  /**
   * @var string[3] 
   */
  protected $bankCurrency;

  /**
   * @var float
   */
  protected $bankFactor;

  /**
   * @var string[255] 
   */
  protected $bankURL;

  /**
   * @var int
   */
  protected $responseCode;

  /**
   * @var string[3]
   */
  protected $responseReasonCode;

  /**
   * @var string[255]
   */
  protected $responseReasonText;

  /**
   * Attribute constructor.
   *
   * @param string $sessionID
   * @param string $returnCode
   * @param string $trazabilityCode
   * @param int $transactionCycle
   * @param string $bankCurrency
   * @param float $bankFactor
   * @param string $bankURL
   * @param int $responseCode
   * @param string $responseReasonCode
   * @param string $responseReasonText
   */
  public function __construct($sessionID, $returnCode, $trazabilityCode, $transactionCycle, 
    $bankCurrency, $bankFactor, $bankURL, $responseCode, $responseReasonCode, $responseReasonText)
  {
    $this->sessionID = $sessionID;
    $this->returnCode = $returnCode;
    $this->trazabilityCode = $trazabilityCode;
    $this->transactionCycle = $transactionCycle;
    $this->bankCurrency = $bankCurrency;
    $this->bankFactor = $bankFactor;
    $this->bankURL = $bankURL;
    $this->responseCode = $responseCode;
    $this->responseReasonCode = $responseReasonCode;
    $this->responseReasonText = $responseReasonText;
  }

  /**
   * @return string
   */
  /*public function getBankCode()
  {
    return $this->bankCode;
  }*/

  /**
   * @return string
   */
  /*public function getBankName()
  {
    return $this->bankName;
  }*/
}