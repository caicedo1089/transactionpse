<?php

namespace App\Soap\Model;

class Authentication
{
  /**
   * @var string[32] 
   */
  protected $login;

  /**
   * @var string[40] SHA1(seed + tranKey)
   */
  protected $tranKey;

  /**
   * @var string
   */
  protected $seed;

  /**
   * @var Attribute[]
   */
  protected $additional;

  /**
   * Attribute constructor.
   *
   * @param string $login
   * @param string $tranKey
   * @param Attribute[] $additional
   */
  public function __construct($login, $tranKey, $additional)
  {
    $this->login = $login;
    $this->seed = date('c');
    $this->tranKey = sha1($this->seed . $tranKey, false); //SHA1(seed + tranKey)
    $this->additional = $additional;
  }

  /**
   * @return string
   */
  public function getLogin()
  {
    return $this->login;
  }

  /**
   * @return string
   */
  public function getSeed()
  {
    return $this->seed;
  }

  /**
   * @return string
   */
  public function getTranKey()
  {
    return $this->tranKey;
  }

  /**
   * @return Attribute[]
   */
  public function getAdditional()
  {
    return $this->additional;
  }
}