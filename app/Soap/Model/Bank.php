<?php

namespace App\Soap\Model;

class Bank
{
  /**
   * @var string[4]
   */
  protected $bankCode;

  /**
   * @var string[60]
   */
  protected $bankName;

  /**
   * Attribute constructor.
   *
   * @param string $bankCode
   * @param string $bankName
   */
  public function __construct($bankCode, $bankName)
  {
    $this->bankCode = $bankCode;
    $this->bankName   = $bankName;
  }

  /**
   * @return string
   */
  public function getBankCode()
  {
    return $this->bankCode;
  }

  /**
   * @return string
   */
  public function getBankName()
  {
    return $this->bankName;
  }
}