<?php

namespace App\Soap\Model;

class PSETransactionResponse
{
  /**
   * @var int
   */
  protected $transactionID;

  /**
   * @var string[32]  
   */
  protected $sessionID;

  /**
   * @var string[32]
   */
  protected $reference;

  /**
   * @var string
   */
  protected $requestDate;

  /**
   * @var string
   */
  protected $bankProcessDate;

  /**
   * @var boolean
   */
  protected $onTest;

  /**
   * @var string[30]
   */
  protected $returnCode;

  /**
   * @var string[40]
   */
  protected $trazabilityCode;

  /**
   * @var int
   */
  protected $transactionCycle;

  /**
   * @var string[20]
   */
  protected $transactionState;

  /**
   * @var int
   */
  protected $responseCode;

  /**
   * @var string[3]
   */
  protected $responseReasonCode;

  /**
   * @var string[255] 
   */
  protected $responseReasonText;

  /**
   * Attribute constructor.
   *
   * @param int $transactionID
   * @param string $sessionID
   * @param string $reference
   * @param string $requestDate
   * @param string $bankProcessDate
   * @param boolean $onTest
   * @param string $returnCode
   * @param string trazabilityCode
   * @param int transactionCycle
   * @param string transactionState
   * @param int $responseCode
   * @param string $responseReasonCode
   * @param string $responseReasonText
   */
  public function __construct($transactionID, $sessionID, $reference, $requestDate, 
    $bankProcessDate, $onTest, $returnCode, $trazabilityCode, $transactionCycle, $transactionState, 
    $responseCode, $responseReasonCode, $responseReasonText)
  {
    $this->transactionID = $transactionID;
    $this->sessionID = $sessionID;
    $this->reference = $reference;
    $this->requestDate = $requestDate;
    $this->bankProcessDate = $bankProcessDate;
    $this->onTest = $onTest;
    $this->returnCode = $returnCode;
    $this->trazabilityCode = $trazabilityCode;
    $this->transactionCycle = $transactionCycle;
    $this->transactionState = $transactionState;
    $this->responseCode = $responseCode;
    $this->responseReasonCode = $responseReasonCode;
    $this->responseReasonText = $responseReasonText;
  }

  /**
   * @return string
   */
  /*public function getBankCode()
  {
    return $this->bankCode;
  }*/

  /**
   * @return string
   */
  /*public function getBankName()
  {
    return $this->bankName;
  }*/
}