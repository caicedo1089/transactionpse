<?php

namespace App\Soap\Model;

class Person
{
  /**
   * @var string[12] 
   */
  protected $document;

  /**
   * @var string[3] [CC, CE, TI, PPN]
   */
  protected $documentType;

  /**
   * @var string[60]
   */
  protected $firstName;

  /**
   * @var string[60] 
   */
  protected $lastName;

  /**
   * @var string[60] 
   */
  protected $company;

  /**
   * @var string[80]  
   */
  protected $emailAddress;

  /**
   * @var string[100]  
   */
  protected $address;

  /**
   * @var string[50]  
   */
  protected $city;

  /**
   * @var string[50] 
   */
  protected $province;

  /**
   * @var string[2]
   */
  protected $country;

  /**
   * @var string[30]  
   */
  protected $phone;

  /**
   * @var string[30] 
   */
  protected $mobile;
  

  /**
   * Attribute constructor.
   *
   * @param string $name
   * @param string $value
   */
  public function __construct($arrData)
  {
    $this->document = $arrData['document']; 
    $this->documentType = $arrData['documentType']; 
    $this->firstName = $arrData['firstName']; 
    $this->lastName = $arrData['lastName']; 
    $this->company = $arrData['company']; 
    $this->emailAddress = $arrData['emailAddress']; 
    $this->address = $arrData['address']; 
    $this->city = $arrData['city']; 
    $this->province = $arrData['province'];  
    $this->country = $arrData['country'];  
    $this->phone = $arrData['phone'];  
    $this->mobile = $arrData['mobile']; 
  }

  /*public function __construct($document, $documentType, $firstName, $lastName, $company, 
    $emailAddress, $address, $city, $province, $country, $phone, $mobile)
  {
    if(is_array($document))
    {
      $arrData = $document;
      $document = $arrData['document']; 
      $documentType = $arrData['documentType']; 
      $firstName = $arrData['firstName']; 
      $lastName = $arrData['lastName']; 
      $company = $arrData['company']; 
      $emailAddress = $arrData['emailAddress']; 
      $address = $arrData['address']; 
      $city = $arrData['city']; 
      $province = $arrData['province'];  
      $country = $arrData['country'];  
      $phone = $arrData['phone'];  
      $mobile = $arrData['mobile']; 
    }

    $this->document = $document; 
    $this->documentType = $documentType; 
    $this->firstName = $firstName; 
    $this->lastName = $lastName; 
    $this->company = $company; 
    $this->emailAddress = $emailAddress;
    $this->address = $address;
    $this->city = $city;
    $this->province = $province;
    $this->country = $country;
    $this->phone = $phone;
    $this->mobile = $mobile;
  }*/

  /**
   * @return string
   */
  public function getDocument()
  {
    return $this->document;
  }

  /**
   * @return string
   */
  public function getDocumentType()
  {
    return $this->documentType;
  }
}