<?php

namespace App\Soap\Model;

class PSETransactionMultiCreditRequest
{
  /**
   * @var string[4]
   */
  protected $bankCode;

  /**
   * @var string[1] 
   */
  protected $bankInterface;

  /**
   * @var string[255] 
   */
  protected $returnURL;

  /**
   * @var string[32]
   */
  protected $reference;

  /**
   * @var string[255]
   */
  protected $description;

  /**
   * @var string[2] 
   */
  protected $language;

  /**
   * @var string[3]
   */
  protected $currency;

  /**
   * @var double
   */
  protected $totalAmount;

  /**
   * @var double
   */
  protected $taxAmount;

  /**
   * @var double
   */
  protected $devolutionBase;

  /**
   * @var double
   */
  protected $tipAmount;

  /**
   * @var Person
   */
  protected $payer;

  /**
   * @var Person
   */
  protected $buyer;

  /**
   * @var Person 
   */
  protected $shipping;

  /**
   * @var string[15] 
   */
  protected $ipAddress;

  /**
   * @var string[255] 
   */
  protected $userAgent;

  /**
   * @var Attribute[]  
   */
  protected $additionalData;

  /**
   * @var CreditConcept[]  
   */
  protected $credits;

  /**
   * Attribute constructor.
   *
   * @param string $bankCode
   * @param string $bankInterface
   * @param string $returnURL
   * @param string $reference
   * @param string $description
   * @param string $language
   * @param string $currency
   * @param double $totalAmount
   * @param double $taxAmount
   * @param double $devolutionBase
   * @param double $tipAmount
   * @param Person $payer
   * @param Person $buyer
   * @param Person $shipping
   * @param string $ipAddress
   * @param string $userAgent
   * @param Attribute[] $additionalData
   * @param CreditConcept[] $credits
   */
  public function __construct($bankCode, $bankInterface, $returnURL, $reference, 
    $description, $language, $currency, $totalAmount, $taxAmount, $devolutionBase, $tipAmount, $payer,
    $buyer, $shipping, $ipAddress, $userAgent, $additionalData, $credits)
  {
    $this->bankCode = $bankCode;
    $this->bankInterface = $bankInterface;
    $this->returnURL = $returnURL;
    $this->reference = $reference;
    $this->description = $description;
    $this->language = $language;
    $this->currency = $currency;
    $this->totalAmount = $totalAmount;
    $this->taxAmount = $taxAmount;
    $this->devolutionBase = $devolutionBase;
    $this->tipAmount = $tipAmount;
    $this->payer = $payer;
    $this->buyer = $buyer;
    $this->shipping = $shipping;
    $this->ipAddress = $ipAddress;
    $this->userAgent = $userAgent;
    $this->additionalData = $additionalData;
    $this->credits = $credits;
  }

  /**
   * @return string
   */
  /*public function getBankCode()
  {
    return $this->bankCode;
  }*/

  /**
   * @return string
   */
  /*public function getBankName()
  {
    return $this->bankName;
  }*/
}