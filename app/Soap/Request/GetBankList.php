<?php

namespace App\Soap\Request;

use App\Soap\model\Authentication;

class GetBankList
{
  /**
   * @var Authentication
   */
  protected $auth;

  /**
   * GetBankList constructor.
   *
   * @param string $auth
   */
  public function __construct($auth)
  {
    $this->auth = $auth;
  }

  /**
   * @return Authentication
   */
  public function getAuth()
  {
    return $this->auth;
  }
}