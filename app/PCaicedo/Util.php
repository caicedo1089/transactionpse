<?php

namespace App\PCaicedo;

use Illuminate\Support\Facades\Input;

class Util
{
    //Datos de autorización
    public static $urlUpdateToken = '';
    public static $arrAuth = [
        'SCOPE' => '',
        'EMAIL_ID' => '',
        'PASSWORD' => ''
    ];
    //Datos necesarios para hacer la consulta
    public static $arrUathURL = [
        'AUTHTOKEN' => '',
        'organization_id' => ''
    ];

    /**
     * Arma la respuesta JSON
     */
     public static function coreJsonResponse($intCode = 500, $message = 'not_support', $data = null )
     {
         $result = [
             'code' => '3',
             'message' => $message
         ];

         if(!is_null($data))
         {
             $result['data'] = $data;
         }

         return response($result, $intCode);
     }

     /**
     * Hace una solicitud
     */
     public static function makeRequest($urlService)
     {
        //Obtenemos la información
        $insRequest = Input::instance();
        
		//creo un nuevo cliente Guzzle
        $client = new \GuzzleHttp\Client();
		
        //Se hace la consulta
        $myResponse = $client->request(
            Input::method(),
            $urlService,
            [
                'query' => array_merge(Util::$arrUathURL, Input::all())
            ]
        );

        //Se responde la respuesta
        return response(
                $myResponse->getBody(), 
                $myResponse->getStatusCode(), 
                [
                    'Content-Type' => 'application/json;charset=UTF-8'
                ]
            );
     }
}
