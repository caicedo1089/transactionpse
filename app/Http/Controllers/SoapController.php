<?php
namespace App\Http\Controllers;

use Artisaninweb\SoapWrapper\SoapWrapper;
use App\Soap\Request\GetConversionAmount;
use App\Soap\Response\GetConversionAmountResponse;

use App\Soap\Request\GetBankList;
use App\Soap\Response\GetBankListResponse;

use App\Soap\Model\Authentication;
use App\Soap\Model\PSETransactionRequest;
use App\Soap\Model\Person;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

//use App\Http\Requests;
use App\PCaicedo\Util;

use Carbon\Carbon;

class SoapController extends Controller
{
    protected $login = '';
    protected $tranKey = '';
    protected $urlWSDL = '';

    protected static $objBanks = null;

    /**
     * @var SoapWrapper
     */
    protected $soapWrapper;

    /**
     * SoapController constructor.
     *
     * @param SoapWrapper $soapWrapper
     */
    public function __construct(SoapWrapper $soapWrapper)
    {
        $this->soapWrapper = $soapWrapper;

        $opts = array(
            'http' => array(
                'user_agent' => 'PHPSoapClient'
            )
        );
        //$this->context = stream_context_create($opts);

        //Agregamos los servicios
        $this->soapWrapper->add(
            'PlacetoPay_PSEService', 
            function ($service) 
            {
                $service
                ->wsdl($this->urlWSDL)
                ->trace(true)
                //->cache(WSDL_CACHE_MEMORY)
                ->options([
                    //'stream_context' => $this->context,
                    'exceptions' => false
                ])
                ->classmap(
                    [
                        //GetConversionAmount::class,
                        //GetConversionAmountResponse::class,

                        GetBankList::class,
                        GetBankListResponse::class,
                    ]
                );
            }
        );
    }

    public function index()
    {
        $this->show();
    }

    public function show() 
    {
        return response(['No soportado'], 200);
    }
    
    public function banks()
    {
        $newDate = null;
        
        if( empty(self::$objBanks) || 
            self::$objBanks->date->diffInDays( ($newDate = Carbon::create()) ) > 0)
        {
            //Hacemos la solicitud
            $auth = new Authentication($this->login, $this->tranKey, []);
            $response = $this->soapWrapper->call(
                'PlacetoPay_PSEService.getBankList',
                [
                    new GetBankList($auth)
                ]
            );

            self::$objBanks = new \stdClass();
            self::$objBanks->date = $newDate;
            self::$objBanks->data = $response->getBankListResult->item;
        }
        
        return response(['data'=> self::$objBanks->data], 200);
    }

    public function createTransaction()
    {
        $insRequest = Input::instance();

        //Agregamos los datos
        $allData = Input::json()->all();
        $allData['ipAddress'] = $insRequest->ip();
        $allData['userAgent'] = $insRequest->header('User-Agent');
        $allData['returnURL'] = 'http://localhost:8000/?reference=' . $allData['reference'];
        
        //Creamos las personas
        $payer = new Person($allData['payer']);
        $buyer = new Person($allData['buyer']);
        $shipping = new Person($allData['shipping']);

        //Creamos loa solicitud
        $tPSERequest = new PSETransactionRequest(
            $allData['bankCode'], $allData['bankInterface'], $allData['returnURL'], $allData['reference'], 
            $allData['description'], $allData['language'], $allData['currency'],
            $allData['totalAmount'], $allData['taxAmount'], $allData['devolutionBase'], $allData['tipAmount'],
            $payer, $buyer, $shipping,
            $allData['ipAddress'], $allData['userAgent'], []
        );

        //Creamos el auth
        $auth = new Authentication($this->login, $this->tranKey, []);

        $objData = new \stdClass();
        $objData->auth = $auth;
        $objData->transaction = $tPSERequest;
        
        $error = null;
        $code = 200;
        
        //Consumimos el Soap
        $response = $this->soapWrapper->call(
            'PlacetoPay_PSEService.createTransaction',
            [
                $objData
            ]
        );

        if (is_soap_fault($response)) 
        {
            $error = $response->faultcode . ' - ' . $response->faultstring;
            $code = 500;
        }

        $arrResponse = [
            'error' => !empty($error),
        ];
        
        if($arrResponse['error'])
        {
            $arrResponse['msg'] = $error;
        }
        else
        {
            $arrResponse['data'] = $response->createTransactionResult;
        }

        return response($arrResponse, $code);
    }

    public function createTransactionMultiCredit()
    {
        $error = null;
        $code = 200;

        $arrResponse = [
            'error' => !empty($error),
            'data' => 'No soportado'
        ];

        return response($arrResponse, $code);
    }

    public function getTransactionInformation($transactionID)
    {
        //Creamos el auth
        $auth = new Authentication($this->login, $this->tranKey, []);

        $objData = new \stdClass();
        $objData->auth = $auth;
        $objData->transactionID = $transactionID;
        
        //Consumimos el Soap
        $response = $this->soapWrapper->call(
            'PlacetoPay_PSEService.getTransactionInformation',
            [
                $objData
            ]
        );

        $error = null;
        $code = 200;

        if(is_soap_fault($response)) 
        {
            $error = $response->faultcode . ' - ' . $response->faultstring;
            $code = 500;
        }

        $arrResponse = [
            'error' => !empty($error),
        ];

        if($arrResponse['error'])
        {
            $arrResponse['msg'] = $error;
        }
        else
        {
            $arrResponse['data'] = $response->getTransactionInformationResult;
        }

        return response($arrResponse, $code);
    }
}