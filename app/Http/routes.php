<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('banks', 'SoapController@banks');

Route::post('createtransaction', 'SoapController@createTransaction');

Route::post('createtransactionmultiCredit', 'SoapController@createTransactionMultiCredit');

Route::get('gettransactioninformation/{transactionID}', 'SoapController@getTransactionInformation');

Route::resource('soap', 'SoapController');

